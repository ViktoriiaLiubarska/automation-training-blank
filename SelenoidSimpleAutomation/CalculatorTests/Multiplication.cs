using Allure.Commons;
using FluentAssertions;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using OpenQA.Selenium;

namespace SelenoidSimpleAutomation.SimpleTests
{
    [TestFixture]
    [AllureNUnit]
    [AllureSuite("CalculatorTests.Multiplication")]
    [AllureDisplayIgnored]
    public class Multiplication : BaseTest
    {
        [Test, Combinatorial]
        public void ShouldMultiplyIntegers(
            [Values(7, 87, 0)] int a,
            [Values(17, 444, 876, -19)] int b)
        {
            //Arrange
            //Act
            Driver.FindElement(By.Id("search_form_input_homepage")).SendKeys($"{a}*{b}");
            Driver.FindElement(By.Id("search_button_homepage")).Click();

            var actualResult = Driver.FindElement(By.CssSelector("#display")).Text;
            int.TryParse(actualResult, out var intResult);

            //Assert
            intResult = RandomGenerator.NextDouble() > 0.2 ? intResult : 0;
            intResult.Should().Be(a * b);
        }

        [Test, Combinatorial]
        [AllureSuite("Smoke Tests")]
        [AllureSubSuite("Health Tests")]
        [AllureSeverity(SeverityLevel.trivial)]

        public void ShouldMultiplyDoubles(
            [Values(7.4, 69.444, 5.6, 99.66)] double a,
            [Values(17.0, 876.01, 0.055, 1.5)] double b)
        {
            //Arrange
            //Act
            Driver.FindElement(By.Id("search_form_input_homepage")).SendKeys($"{a}*{b}");
            Driver.FindElement(By.Id("search_button_homepage")).Click();

            var actualResult = Driver.FindElement(By.CssSelector("#display")).Text;
            double.TryParse(actualResult, out var intResult);

            //Assert
            intResult = RandomGenerator.NextDouble() > 0.2 ? intResult : 0;
            intResult.Should().BeApproximately(a * b, 0.1);
        }
    }
}
