﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Allure.Commons;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace SelenoidSimpleAutomation
{
    public class BaseTest
    {
        protected IWebDriver Driver;
        protected Random RandomGenerator;

        [OneTimeSetUp]
        public void Init()
        {
            RandomGenerator = new Random();
            Environment.SetEnvironmentVariable(
                AllureConstants.ALLURE_CONFIG_ENV_VARIABLE,
                Path.Combine(Environment.CurrentDirectory, AllureConstants.CONFIG_FILENAME));

            #region Selenoid_Setup_Here
            var seleniumServer = "http://nas:password@irongate.pro:4444/wd/hub";

            var capabilities = new DesiredCapabilities();
            capabilities.Platform = new Platform(PlatformType.Any);
            capabilities.SetCapability(CapabilityType.BrowserName, "chrome");
            capabilities.SetCapability(CapabilityType.Version, "87.0");

            //Selenoid attribute samples
            capabilities.SetCapability("enableVNC", true);
            capabilities.SetCapability("enableVideo", false);
            capabilities.SetCapability("screenResolution", "1920x1080x24");

            Driver = new RemoteWebDriver(new Uri(seleniumServer), capabilities, TimeSpan.FromMinutes(5));
            #endregion

            //Driver = new ChromeDriver();

            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Driver.Manage().Window.Maximize();
        }

        [SetUp]
        public void Setup()
        {
            Driver.Url = "https://duckduckgo.com/";
        }

        [TearDown]
        public void TakeScreens()
        {
            if (TestContext.CurrentContext.Result.Outcome == ResultState.Success) return;
            if (!Driver.Url.Any()) return;

            var allureInstance = AllureLifecycle.Instance;

            allureInstance.AddAttachment("Test browser logs",
                "application/txt",
                Encoding.ASCII.GetBytes(TestContext.CurrentContext.Result.StackTrace ?? string.Empty),
                ".txt");

            allureInstance.AddAttachment("ScreenShot", "image/png", MakeScreenShot());
        }

        private string MakeScreenShot()
        {
            var ss = ((ITakesScreenshot)Driver).GetScreenshot();
            var test = TestContext.CurrentContext.Test.Name;
            var resultsPath = AllureLifecycle.Instance.ResultsDirectory;
            var screenPath = $"{resultsPath}\\{test}.png";
            ss.SaveAsFile(screenPath);
            return screenPath;
        }

        [OneTimeTearDown]
        public void Dispose()
        {
            Driver.Dispose();
        }
    }
}